Module = { Version = { full = { 1, 0, 2, '' } } }

---
--- Created by Rostriano
--- Date: 2024-08-08
---

-- Versioning --
Module.Version.pretty = EEPROM.Version.ToString( Module.Version.full )
--[[ version history
n/a
]]

Vector2d = require( "/lib/Vector2d.lua", EEPROM.Remote.CommonLib )
require( "/BatteryInfo.lua" )
require( "/Footer.lua" )

local PowerMonitor = {
    connector = nil,
    gpu = nil,
    pollInterval = 1,

    productionList = nil,
    consumptionList = nil,
    maxPowerConsumptionList = nil,

    powerGraph = nil,
    footer = nil,

    colors = {
        consumption = Color.FICSIT_ORANGE,
        capacity = Color.GREY_0500,
        production = Color.GREY_0750,
        maxConsumption = Color.new( 0.050, 0.500, 0.700, 1.0 ),
    },

    graphWidth = 2300,
    graphHeight = 1550,
}

--------------------------------------------------------------------------------

function PowerMonitor:init( power, gpu )
    print( "\nInitialising PowerMonitor " .. Module.Version.pretty .. "\n" )

    self.gpu = gpu
    local connectors = power:getPowerConnectors()
    if #connectors == 0 then
        computer.panic( 'The power interface has no connectors, cannot continue' )
    end
    self.connector = connectors[1]

    self:initLists()

    -- Screen size is 3000x1800
    self.powerGraph = srf.Graphics.Graph.Graph.new()
    self.powerGraph:setDimensions( Vector2d.new( self.graphWidth,self.graphHeight ) )
    self.powerGraph:addPlotter(
        'consumption',
        {
            gpu = self.gpu,
            position = Vector2d.new( 0,0 ),
            color = self.colors.consumption,
            dataSource = self.consumptionList,
        }
    )
    self.powerGraph:addPlotter(
        'capacity',
        {
            gpu = self.gpu,
            position = Vector2d.new( 0,0 ),
            color = self.colors.capacity,
            dataSource = self.capacityList,
        }
    )
    self.powerGraph:addPlotter(
        'production',
        {
            gpu = self.gpu,
            position = Vector2d.new( 0,0 ),
            color = self.colors.production,
            dataSource = self.productionList,
        }
    )
    self.powerGraph:addPlotter(
        'maxConsumption',
        {
            gpu = self.gpu,
            position = Vector2d.new( 0,0 ),
            color = self.colors.maxConsumption,
            dataSource = self.maxPowerConsumptionList,
        }
    )

    self.batteryInfo = srf.PowerMonitor.BatteryInfo:new({ connector = self.connector })
    self.batteryInfo:init( self.gpu, Vector2d.new( self.graphWidth,0 ), Vector2d.new( 3000-self.graphWidth,1800  ) )

    self.footer = srf.PowerMonitor.Footer:new({colors = self.colors})
    self.footer:init( self.gpu, Vector2d.new( 0, self.graphHeight ), Vector2d.new( self.graphWidth, 1000 ) )
end

function PowerMonitor:run()
    print( "PowerMonitor running")

    while true do
        --[[
            CIRCUIT PARAMS
            --------------
            production
            consumption
            capacity
            batteryInput
            maxPowerConsumption
            isFuesed
            hasBatteries
            batteryCapacity
            batteryStore
            batteryStorePercent
            batteryTimeUntilFull
            batteryTimeUntilEmpty
            batteryIn
            batteryOut
        ]]

        self:collectData()

        -- Paint background
        -- self.gpu:drawRect( Vector2d.new(0,0), Vector2d.new(3000,1800), Color.WHITE, nil, nil )

        self.gpu:drawLines(
            { Vector2d.new( 0,self.graphHeight ), Vector2d.new( self.graphWidth,self.graphHeight ) },
            5,
            Color.GREY_0500
        )
        self.gpu:drawLines(
            { Vector2d.new( self.graphWidth,0 ), Vector2d.new( self.graphWidth,self.graphHeight ) },
            5,
            Color.GREY_0500
        )

        self.powerGraph:draw()

        self.footer:setValues({
            production = self.production,
            capacity = self.capacity,
            consumption = self.consumption,
            maxPowerConsumption = self.maxPowerConsumption,
        })
        self.footer:draw()

        self.batteryInfo:draw()


        self.gpu:flush()

        event.pull( self.pollInterval )
    end
end

function PowerMonitor:initLists()
    -- TODO: dynamically determine nr of data points

    self.productionList = srf.DataStructures.SizeLimitedList.new( 100 )
    self.capacityList = srf.DataStructures.SizeLimitedList.new( 100 )
    self.consumptionList = srf.DataStructures.SizeLimitedList.new( 100 )
    self.maxPowerConsumptionList = srf.DataStructures.SizeLimitedList.new( 100 )
end
--
function PowerMonitor:collectData()
    -- We get the circuit inside the loop so that stuff doesn't crash whenever a wire is detached
    local circuit = self.connector:getCircuit()

    self.production = ( circuit and circuit.production ) or 0
    self.capacity = ( circuit and circuit.capacity ) or 0
    self.consumption = ( circuit and circuit.consumption ) or 0
    self.maxPowerConsumption = ( circuit and circuit.maxPowerConsumption ) or 0

    self.productionList:add( self.production )
    self.capacityList:add( self.capacity )
    self.consumptionList:add( self.consumption )
    self.maxPowerConsumptionList:add( self.maxPowerConsumption )
end

createModule( 'srf.PowerMonitor.PowerMonitor', PowerMonitor )
