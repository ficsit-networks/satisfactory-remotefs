---
--- Created by Rostriano
--- 2024-08-08

require( "/Debug.lua", EEPROM.Remote.DeveloperTools )
Color = require( "/lib/Colors.lua", EEPROM.Remote.CommonLib )
require( "/lib/Vector2d.lua", EEPROM.Remote.CommonLib )

-- TODO: The old code above needs to be made strict
require( "/lib/strict.lua", EEPROM.Remote.CommonLib )

require( "/dataStructures/SizeLimitedList.lua", EEPROM.Remote.CommonLib )
require( "/Graphics/Graph/Graph.lua", EEPROM.Remote.CommonLib )
require( "/Graphics/Graph/Graph.lua", EEPROM.Remote.CommonLib )
require( "PowerMonitor.lua" )
ClassGroup = require( "/lib/classgroups.lua", EEPROM.Remote.CommonLib )


local power = component.getComponentsByClass( ClassGroup.Power )
if #power == 0 then
    computer.panic( "No power pole or wall outlet hooked up; nothing to monitor" )
end
power = power[1]

local gpu = computer.getPCIDevices( classes.GPU_T2_C )[1]
if gpu == nil then
    computer.panic( "No GPU T2 found. Cannot continue." )
end

local screens = component.getComponentsByClassAndNick( ClassGroup.Displays.Screens, EEPROM.Boot.ComputerSettings.screen or '' )
if #screens == 0 then
    computer.panic( "No screen found. Cannot continue." )
end

gpu:bindScreen( screens[1] )

local screenSize = gpu:getScreenSize()
print( 'Screen resolution: ' .. screenSize.x .. 'x' .. screenSize.y )

local powerMonitor = srf.PowerMonitor.PowerMonitor
powerMonitor:init( power, gpu )
powerMonitor:run()
