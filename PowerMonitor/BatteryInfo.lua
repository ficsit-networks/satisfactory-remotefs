
require( "/Graphics/ScreenElement.lua", EEPROM.Remote.CommonLib )

local BatteryInfo = srf.Graphics.ScreenElement:new()
BatteryInfo.line = 0
BatteryInfo.lineHeight = 60
BatteryInfo.fontSize = 35
BatteryInfo.textColor = Color.GREY_0750
BatteryInfo.dataFontSize = 50
BatteryInfo.dataColor = Color.WHITE
BatteryInfo.colorBad = Color.RED
BatteryInfo.colorGood = Color.new( 0.000, 1.000, 0.000, 1.0 )

function BatteryInfo:draw()
        --[[
            CIRCUIT PARAMS
            --------------
            batteryInput
            hasBatteries
            batteryCapacity
            batteryStore
            batteryStorePercent
            batteryTimeUntilFull
            batteryTimeUntilEmpty
            batteryIn
            batteryOut
        ]]
    -- We get the circuit inside the loop so that stuff doesn't crash whenever a wire is detached
    local circuit = self.connector:getCircuit()

    if not circuit.hasBatteries then
        return
    end

    local batteryCapacity = ( circuit and circuit.batteryCapacity ) or 0
    local batteryStore = ( circuit and circuit.batteryStore ) or 0
    local batteryStorePercent = ( circuit and 100 * circuit.batteryStorePercent ) or 0
    local batteryTimeUntilFull = ( circuit and circuit.batteryTimeUntilFull ) or 0
    local batteryTimeUntilEmpty = ( circuit and circuit.batteryTimeUntilEmpty ) or 0
    local batteryIn = ( circuit and circuit.batteryIn ) or 0
    local batteryOut = ( circuit and circuit.batteryOut ) or 0

    self.line = 0

    self:print( 'Stored' )
    self:print( string.format( '%.1f %%', batteryStorePercent ), self.dataFontSize, self.percentageColor( batteryStorePercent ) )
    self.line = self.line + 2

    self:print( 'Charge' )
    if( batteryStorePercent == 100 ) then
        self:print( string.format( '%.1f MWh', batteryStore ), self.dataFontSize, self.percentageColor( batteryStorePercent ) )
    else
        self:print( string.format( '%.1f / %.1f MWh', batteryStore, batteryCapacity ), self.dataFontSize, self.percentageColor( batteryStorePercent ) )
    end
    self.line = self.line + 2

    if batteryOut > 0 then
        self:print( 'Discharge rate' )
        self:print( string.format( '%.1f MW', batteryOut ), self.dataFontSize, self.colorBad )
        self.line = self.line + 2
    elseif batteryIn > 0 then
        self:print( 'Charge rate' )
        self:print( string.format( '%.1f MW', batteryIn ), self.dataFontSize, self.colorGood )
        self.line = self.line + 2
    end

    if batteryTimeUntilEmpty > 0 then
        self:print( 'Time until empty ' )
        self:print( self.formatTime( batteryTimeUntilEmpty ), self.dataFontSize, self.colorBad )
        self.line = self.line + 2
    elseif batteryTimeUntilFull > 0 then
        self:print( 'Time until full' )
        self:print( self.formatTime( batteryTimeUntilFull ), self.dataFontSize, self.colorGood )
        self.line = self.line + 2
    end
end

function BatteryInfo:print( text, size, color )
    self.line = self.line + 1

    local yPos = self.line * self.lineHeight

    size = size or self.fontSize
    color = color or self.textColor
    self:drawText( Vector2d.new( 40,yPos ), text, size, color)
end

function BatteryInfo.formatTime( seconds )
    return string.format(
      "%02d:%02d:%02d",
      math.floor( seconds / 3600 ) % 24,
      math.floor( seconds / 60 ) % 60,
      math.floor( seconds % 60 )
    )
end

function BatteryInfo.percentageColor( percentage )
    if percentage < 33 then
        return Color.RED
    elseif percentage < 80 then
        return Color.FICSIT_ORANGE
    elseif percentage < 100 then
        return Color.GREEN
    else
        return Color.GREY_0750
    end
end

createModule( 'srf.PowerMonitor.BatteryInfo', BatteryInfo )
