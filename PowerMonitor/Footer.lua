
require( "/Graphics/ScreenElement.lua", EEPROM.Remote.CommonLib )

local Footer = srf.Graphics.ScreenElement:new()
Footer.fontSize = 50
Footer.textColor = Color.GREY_0750
Footer.textVerticalOffset = -22

function Footer:draw()
    self:drawRect( Vector2d.new( 100,50 ), Vector2d.new( 50,50 ), self.colors.consumption, nil, nil )
    self:drawText( Vector2d.new( 200,50+self.textVerticalOffset ), self._getLabel( "Consumption", self.values.consumption), self.fontSize, self.textColor)

    self:drawRect( Vector2d.new( 100,150 ), Vector2d.new( 50,50 ), self.colors.production, nil, nil )
    self:drawText( Vector2d.new( 200,150+self.textVerticalOffset ), self._getLabel( "Production", self.values.production), self.fontSize, self.textColor)

    self:drawRect( Vector2d.new( 1100,50 ), Vector2d.new( 50,50 ), self.colors.maxConsumption, nil, nil )
    self:drawText( Vector2d.new( 1200,50+self.textVerticalOffset ), self._getLabel( "Max. consumption", self.values.maxPowerConsumption), self.fontSize, self.textColor)

    self:drawRect( Vector2d.new( 1100,150 ), Vector2d.new( 50,50 ), self.colors.capacity, nil, nil )
    self:drawText( Vector2d.new( 1200,150+self.textVerticalOffset ), self._getLabel( "Production capacity", self.values.capacity), self.fontSize, self.textColor)
end

function Footer:setValues(values)
    self.values = values
end

function Footer._getLabel( text, value )
    if value == nil then
        value = 'NaN'
    else
        value = string.format( '%.1f', value )
    end

    return text .. ' ' .. value .. ' MW'
end

createModule( 'srf.PowerMonitor.Footer', Footer )
