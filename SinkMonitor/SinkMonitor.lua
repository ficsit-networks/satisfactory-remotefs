Module = { Version = { full = { 0, 0, 1, '' } } }

---
--- Created by Rostriano
--- Date: 2024-04-15
---

-- Versioning --
Module.Version.pretty = EEPROM.Version.ToString( Module.Version.full )
--[[ version history
n/a
]]

Vector2d = require( "/lib/Vector2d.lua", EEPROM.Remote.CommonLib )



local SinkMonitor = {
    sink = nil,
    gpu = nil,

    pointsList = nil,
    pointsToNextCouponList = nil,
    couponProgressList = nil,
    nrCouponsList = nil,
    nrCoupons = nil,

    pointsGraph = nil,
    pointsToNextCouponGraph = nil,
    couponProgressGraph = nil,
}

createModule( 'srf.SinkMonitor', SinkMonitor )

--------------------------------------------------------------------------------


function SinkMonitor:init( sink, gpu )
    print( "\nInitialising SinkMonitor " .. Module.Version.pretty .. "\n" )
    self.sink = sink
    self.gpu = gpu

    self.nrCoupons = nil
    self:initLists()

    self.pointsGraph = srf.Graphics.Graph.Graph:new()
    self.pointsGraph:init( self.gpu, Vector2d.new( 0,0 ), Vector2d.new( 1000,1000 ) )
    self.pointsGraph:setData( self.pointsList )

    -- self.pointsGraph:setMaxVal( 1 )
end

function SinkMonitor:run()

    print( "SinkMonitor running")

    while true do
        -- print( 'nrpoints ' .. self.sink.couponProgress )
        -- self.pointsList:add( self.sink.couponProgress )

        print( 'nrpoints ' .. self.sink.numPoints )
        self.pointsList:add( self.sink.numPoints )
        -- self.pointsToNextCouponList:add( self.sink.numPointsToNextCoupon )
        -- self.couponProgressList:add( self.sink.couponProgress )
        -- self.nrCouponsList:add( self.sink.numCoupons )
        -- self.nrCoupons = self.sink.numCoupons


        -- print_r(self.pointsList)
        -- print( self.pointsList:getMinVal(),  self.pointsList:getMaxVal() )


        self.pointsGraph:draw()
        -- self.pointsToNewCouponGraph:draw()
        -- self.couponProgressGraph:draw()

        self.gpu:flush()

        event.pull( 1 )
    end
end

function SinkMonitor:initLists()
    -- TODO: dynamically determine nr of data points

    self.pointsList = srf.DataStructures.SizeLimitedList.new(100)
    self.pointsToNextCouponList = srf.DataStructures.SizeLimitedList.new(100)
    self.couponProgressList = srf.DataStructures.SizeLimitedList.new(100)
    self.nrCouponsList = srf.DataStructures.SizeLimitedList.new(100)
end








-- while true do
--     pointsList:add( sink.numPoints )
--     pointsToNextCouponList:add( sink.numPointsToNextCoupon )
--     couponProgressList:add( sink.couponProgress )
--     nrCoupons = sink.numCoupons

--     -- print(nrCoupons)


--     event.pull( 10 )
-- end


-- --[[
--     #points 484603936
--     #coupons 21
--     #numPointsToNextCoupon 3319564
--     couponProgress 0.32274529337883
-- ]]

