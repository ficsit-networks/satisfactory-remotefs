---
--- Created by Rostriano
---

require( "/Debug.lua", EEPROM.Remote.DeveloperTools )
Color = require( "/lib/Colors.lua", EEPROM.Remote.CommonLib )
require( "/lib/Vector2d.lua", EEPROM.Remote.CommonLib )

-- TODO: The old code above needs to be made strict
require( "/lib/strict.lua", EEPROM.Remote.CommonLib )

require( "/dataStructures/SizeLimitedList.lua", EEPROM.Remote.CommonLib )
require( "/Graphics/Graph/Graph.lua", EEPROM.Remote.CommonLib )
require( "/Graphics/Graph/Graph.lua", EEPROM.Remote.CommonLib )
-- require( "/Graphics/Graph/Axes.lua", EEPROM.Remote.CommonLib )
require( "SinkMonitor.lua" )
ClassGroup = require( "/lib/classgroups.lua", EEPROM.Remote.CommonLib )

-- local plotter = Plot.new()
-- local list = SizeLimitedList.new()

-- plotter:setData( list )
-- plotter:print()
-- list:add(1)
-- plotter:print()
-- list:add(2)
-- plotter:print()







local sinks = component.getComponentsByClass( ClassGroup.ResourceSinks )
if #sinks == 0 then
    computer.panic( "No A.W.E.S.O.M.E. Sink hooked up; nothing to monitor" )
-- elseif #sinks > 1 then
    -- print( "There are multiple A.W.E.S.O.M.E. Sinks hooked up. There's no point in doing that." )
end
local sink = sinks[1]

local gpu = computer.getPCIDevices(classes.GPU_T2_C)[1]
if gpu == nil then
    computer.panic( "No GPU T2 found. Cannot continue." )
end

local comp = component.findComponent(classes.Screen)[1]
if not comp then
    computer.panic("No Screen found!")
end
screen = component.proxy(comp)

if screen == nil then
    computer.panic( "No screen found. Cannot continue." )
end

gpu:bindScreen( screen )

local sinkMonitor = srf.SinkMonitor
sinkMonitor:init( sink, gpu )
sinkMonitor:run()


-- -- sizeX, sizeY = screen:getSize()
-- -- print('screen size ', sizeX, sizeY)

-- local position = Vector2d.new( 0, 0 )
-- local dimensions = Vector2d.new( 400, 300 )

-- local element = srf.Graphics.ScreenElement:new()
-- element:init( gpu, position, dimensions )


-- -- local scaledVector = element:scaleSize( Vector2d.new( 40, 30 ) )
-- -- local scaledVector = element:scalePosition( Vector2d.new( 40, 30 ) )
-- -- print_r(scaledVector)

-- print('screen')
-- print_r(screen)
--

-- local points = {
--     Vector2d.new(0,0),
--     Vector2d.new(100,100),
--     Vector2d.new(200,200),
--     Vector2d.new(400,300),
-- }

-- element:drawLines( points, 10, Color.GREY_0500 )
-- element:drawText( Vector2d.new(100, 100), "Hola todos los amigos y amigas!", 40, Color.BLUE, true )


-- element:draw()
-- gpu:flush()


