--[[
    Try to figure out which index the various elements of an RSS sign have
]]


if EEPROM.Boot.Disk ~= nil and EEPROM.Boot.Disk ~= '' then
    -- A disk in the computer means the player
    -- will want a full log of events on disk.
    require( "/lib/ConsoleToFile.lua", EEPROM.Remote.CommonLib )
end


CurrElementIndex = 0
PrevElementIndex = 0
NrElements = nil
ColorOptions = {}
ColorOptionIndex = 1

require( "/Debug.lua" )
local ClassGroup = require( "/lib/classgroups.lua", EEPROM.Remote.CommonLib )
local Color = require( "/lib/Colors.lua", EEPROM.Remote.CommonLib )
local UIO = require( "/UIO/UIO.lua", EEPROM.Remote.CommonLib )
require( "/UIO/UIOElements/Combinator.lua", EEPROM.Remote.CommonLib )
require( "/UIO/UIOElements/Extensions.lua", EEPROM.Remote.CommonLib )
require( "/UIO/UIOElements/RSSElement.lua", EEPROM.Remote.CommonLib )
require( "/UIO/UIOElements/ButtonModule.lua", EEPROM.Remote.CommonLib )


local function addColorOption( colorOptions, color)
    table.insert(colorOptions, color)

    return colorOptions
end

--- Red
addColorOption(
    ColorOptions,
    Color.new( 0.3      , 0.0       , 0.0       , 1.0 )
)
--- Green
addColorOption(
    ColorOptions,
    Color.new( 0.0      , 0.3       , 0.0       , 1.0 )
)
--- Blue
addColorOption(
    ColorOptions,
    Color.new( 0.0      , 0.0       , 0.3       , 1.0 )
)
--- Black
addColorOption(
    ColorOptions,
    Color.new( 1,0      , 1.0       , 1.0       , 1.0 )
)
CurrColor = ColorOptions[1]




---Create a Button Module or panic
---@param button userdata The Button Module
---@return UIO.UIOElements.ButtonModule|nil
local function createButtonModule( button )
    local uio = UIO.UIOElements.ButtonModule.create( button )
    if uio == nil then
        computer.panic( "Internal Software Error: Unable to create UIO.UIOElements.ButtonModule" )
    end
    return uio
end


local signs = component.getComponentsByClass( ClassGroup.Displays.Signs.ReallySimpleSigns.All )
if signs == nil or #signs == 0 then
    computer.panic( "No Really Simple Sign found" )
end
Sign = signs[1]
NrElements = Sign:getNumOfElements()

local panels = component.getComponentsByClass( ClassGroup.ModulePanels.All )
if panels == nil or #panels == 0 then
    computer.panic( "No Module Panel!" )
end


local function createUserControlCombinator(panels, buttonX, buttonY)
    local combined = {}

    if panels ~= nil then
        for _, panel in pairs( panels ) do
            local button = panel:getModule( buttonX, buttonY )

            if button ~= nil then
                local uio = createButtonModule( button )
                table.insert( combined, uio )
            else
                print("Button not found: " .. buttonX .. ", " .. buttonY)
            end
        end
    end


    local combinator = UIO.UIOElements.Combinator.create( combined )
    UIO.UIOElement.Extensions.AddBoolStateControl( combinator )
    UIO.UIOElement.Extensions.AddSignalBlockControl( combinator )

    return combinator
end

local function triggerButtonDown( edata )
    computer.beep()

    PrevElementIndex = CurrElementIndex
    CurrElementIndex = CurrElementIndex - 1

    if CurrElementIndex < 0 then
      CurrElementIndex = NrElements
    end

    selectElement()
end

local function triggerButtonUp( edata )
    computer.beep()

    PrevElementIndex = CurrElementIndex
    CurrElementIndex = CurrElementIndex + 1

    if CurrElementIndex > NrElements then
      CurrElementIndex = 0
    end

    selectElement()
end

local function triggerButtonColorChange( edata )
    computer.beep()

    ColorOptionIndex = ColorOptionIndex + 1

    if ColorOptionIndex > #ColorOptions then
      ColorOptionIndex = 1
    end
    CurrColor = ColorOptions[ ColorOptionIndex ]

    selectElement()
end


function selectElement()
    Sign:Element_SetColor({0, 0, 0.5, 0, 5}, PrevElementIndex)
    Sign:Element_SetColor(CurrColor, CurrElementIndex)
    print("Selected element: " .. CurrElementIndex)
end

---Dispatch events to the proper subsystem
function HandleEvent( edata )
    if edata == nil then return false end
    if edata[ 1 ] == nil then return false end
    if edata[ 2 ] == nil then return false end

    if UIO.UIOElements:eventHandler( edata ) then
        return true
    end

    return false
end

-- Get the first Button on the Panel
local downButton = createUserControlCombinator(panels, 0, 0)
if not downButton:setSignalHandler( "Trigger", triggerButtonDown ) then
    HypertubeNode.panicNode( true, "Internal Software Error", "Could not register for 'Trigger' signal on buttonDown" )
end


-- Get the second Button on the Panel
local upButton = createUserControlCombinator(panels, 1, 0)
if not upButton:setSignalHandler( "Trigger", triggerButtonUp ) then
    HypertubeNode.panicNode( true, "Internal Software Error", "Could not register for 'Trigger' signal on buttonUp" )
end
--
-- Get the third Button on the Panel
local colorChangeButton = createUserControlCombinator(panels, 2, 0)
if not colorChangeButton:setSignalHandler( "Trigger", triggerButtonColorChange ) then
    HypertubeNode.panicNode( true, "Internal Software Error", "Could not register for 'Trigger' signal on buttonColorChange" )
end


computer.beep() --- Signify ready state
while true do
    local edata = { event.pull() }

    HandleEvent( edata )
end
