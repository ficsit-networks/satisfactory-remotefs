
Module = { Version = { full = { 0, 0, 1, '' } } }

---
--- Created by Rostriano
--- Date: 2024-04-17
---

-- Versioning --
Module.Version.pretty = EEPROM.Version.ToString( Module.Version.full )
--[[ version history
n/a
]]


if EEPROM.Boot.Disk ~= nil and EEPROM.Boot.Disk ~= '' then
    -- A disk in the Hypertube Node computer means the player is doing some
    -- maintenance on the network and will want a full log of events.
    require( "/lib/ConsoleToFile.lua", EEPROM.Remote.CommonLib )
end


print( "\nInitialising DaysSincePlanetfall " .. Module.Version.pretty .. "\n" )

local Color = require( "/lib/Colors.lua", EEPROM.Remote.CommonLib )
local UIO = require( "/lib/UIO.lua" )
local RSSBuilder = require( "/lib/RSSBuilder.lua" )

-- local ClassGroup = require( "/lib/classgroups.lua", EEPROM.Remote.CommonLib )
-- local UIO = require( "/UIO/UIO.lua", EEPROM.Remote.CommonLib )
-- require( "/UIO/UIOElements/Combinator.lua", EEPROM.Remote.CommonLib )
-- require( "/UIO/UIOElements/RSSElement.lua", EEPROM.Remote.CommonLib )
-- local Vector2f = require( "/lib/Vector2f.lua", EEPROM.Remote.CommonLib )



------------------------------------------------------------------------------------------


-- How to order the element[s] on the RSS sign[s]
local RSS_EZINDEX_TEXT                      =  64       -- In front List/Map
local RSS_EZINDEX_MAIN_IMAGE                =  40       -- In front of everything
local RSS_EZINDEX_BLACKGROUND               =  -1       -- Hiding that which we do not want to see
local RSS_EZINDEX_HIDDEN                    = -64       -- Behind everything


local ____nextRSSElementIndex                   = 0         -- Don't trust my own ability to track these, do it automagically
local function nextRSSIndex( extra )
    if extra == nil or type( extra ) ~= "number" or extra < 0 then
        extra = 0
    end
    local result = ____nextRSSElementIndex
    ____nextRSSElementIndex = ____nextRSSElementIndex + 1 + extra
    return result
end

-- Where on the RSS sign[s] to find the element[s]
local RSS_EID_TEXT_1                       = nextRSSIndex( 0 ) -- Text
local RSS_EID_TEXT_2                       = nextRSSIndex( 0 ) -- Text
local RSS_EID_TEXT_3                       = nextRSSIndex( 0 ) -- Text
local RSS_EID_IMG_LOGO_HAT                 = nextRSSIndex( 0 ) -- Text
local RSS_EID_IMG_FICSIT                   = nextRSSIndex( 0 ) -- Text
local RSS_EID_IMG_DOGGO                    = nextRSSIndex( 0 ) -- Text
local RSS_EID_IMG_BLACKGROUND              = nextRSSIndex( 0 ) -- Text


--------------------------------------------------------------------------------


local signs = {
    square = component.getComponentsByClassAndNick( "Build_RSS_1x1_C" , "DaysSincePlanetfall" )
    -- square = component.getComponentsByClassAndNick( ClassGroup.Displays.Signs.Square.All , "DaysSincePlanetfall" )
    -- tall1x2 = component.getComponentsByClassAndNick( ClassGroup.Displays.Signs.Tall1x2.All , "DaysSincePlanetfall" )
    -- wide2x1 = component.getComponentsByClassAndNick( ClassGroup.Displays.Signs.Wide2x1.All , "DaysSincePlanetfall" )

}
signs = signs.square -- XXXX

if #signs == 0 then
    computer.panic("No RSS screens found")
end

print ("Nr screens: " .. #signs)

local sign = signs[1]
print ("Nr elements:", sign:getNumOfElements())
-- sign = signs[2]
-- print ("Nr elements:", sign:getNumOfElements())




-- Color.RED = Color.new( 0.7, 0, 0, 0 )
-- Color.GREEN = Color.new( 0, 0.7, 0, 0 )
-- Color.BLUE = Color.new( 0, 0, 0.7, 0 )

Color.FICSIT_LOGO_BLACKGROUND = Color.new( 0.025, 0.025, 0.025, 1 )

-- print("sign:", sign.location.x, sign.location.y, sign.location.z)


local RSS_EID_TEXT_LINE_1                         = 3
local RSS_EID_BLACKGROUND                       = 6
TextLine1 = UIO.createSimpleDisplayCombinator( signs, RSS_EID_TEXT_LINE_1 )
Blackground = UIO.createSimpleDisplayCombinator( signs, RSS_EID_BLACKGROUND )



-- TextLine1:setText( "Hallo wereldje!" )
-- TextLine1:setForeColor( Color.RED )

-- Blackground:setForeColor( Color.BLACK )


-- TextLine1:setZIndex( 2 )






-- local mOverwriteImageSize = Vector2f.new( 48.000000, 48.000000 )
-- local mTexture = "/RSS/Assets/Images/Milestones/rss_milestone.rss_milestone"
-- local mUrlPattern = '%s%s/images/%s'
-- local mUrl = string.format( mUrlPattern, EEPROM.Remote.FSBaseURL, EEPROM.Remote.CommonLib, "ANS-E-Profile.png" )
-- local mPosition = Vector2f.new( 352.000000, 224.000000 )
-- if not addRSSImageElementToLayout( layout, RSS_EID_FILLER + 1, "icnLogoMe"   , RSS_EZINDEX_FILLER, mPosition, mTexture, nil, mOverwriteImageSize, nil, mUrl ) then return nil end


-- local layout = {}
-- layout.signSize="1x1"
-- layout.elements = {}
-- layout.mFlatData = nil
-- layout.mMaterialData = nil
-- layout.mHologramData = nil

local layout = RSSBuilder.SignLayout.new( { signSize = "1x1" } )


local mPosition = Vector2f.new( 0.000000, 200.000000 )
-- local mOverwriteImageSize = Vector2f.new( 768.000000, 96.000000 )
-- local bdrTexture = "/RSS/Assets/Images/UI/Shapes/7x1/build_7x1_holo.build_7x1_holo"
-- local bdrUse9SliceModeStatus = Vector2f.new( 1.000000, 0.010000 )
-- local bgdTexture = "/KUI/Assets/9Slice/9S1111.9S1111"
-- local bgdUse9SliceModeStatus = Vector2f.new( 1.000000, 0.250000 )
if not RSSBuilder.addRSSTextElementToLayout ( layout, RSS_EID_TEXT_1     , "txtStatus", RSS_EZINDEX_TEXT    , mPosition, "Initializing...", 15, RSSBuilder.Text.Justification.Middle ) then return nil end
-- if not RSSBuilder.addRSSImageElementToLayout( layout, RSS_EID_STATUS_TEXT +  1, "bdrStatus", RSS_EZINDEX_STATUS_FRONT - 1, mPosition, bdrTexture, Color.GREY_0750    , mOverwriteImageSize, bdrUse9SliceModeStatus ) then return nil end
-- if not RSSBuilder.addRSSImageElementToLayout( layout, RSS_EID_STATUS_TEXT +  2, "bgdStatus", RSS_EZINDEX_STATUS_FRONT - 2, mPosition, bgdTexture, Color.CYAN_SIGN_LOW, mOverwriteImageSize, bgdUse9SliceModeStatus ) then return nil end

print_r(layout)
