
local RSSBuilder = require( "/lib/RSSBuilder.lua", EEPROM.Remote.CommonLib )
-- local UIO = require( "/UIO/UIO.lua", EEPROM.Remote.CommonLib )
-- require( "/UIO/UIOElements/RSSElement.lua", EEPROM.Remote.CommonLib )


---Create a Text Element for an RSSBuilder SignLayout
RSSBuilder.addRSSTextElementToLayout = function( layout, eIndex, mElementName, mZIndex, mPosition, mText, mTextSize, mTextJustify, mColourOverwrite, mPadding )
    local mSharedData, reason = RSSBuilder.SignLayout.ElementData.SharedData.new( {
        mElementName = mElementName,
        mPosition = mPosition,
        mZIndex = mZIndex,
        mColourOverwrite = mColourOverwrite,
    } )
    if mSharedData == nil then
        computer.panic( "Could not create SignLayout: " .. reason or "" )
        return false
    end

    local mTextData, reason = RSSBuilder.SignLayout.ElementData.TextData.new( {
        mText = mText,
        mTextSize = mTextSize,
        mTextJustify = mTextJustify,
        mPadding = mPadding,
    } )
    if mTextData == nil then
        computer.panic( "Could not create SignLayout: " .. reason or "" )
        return false
    end

    local mElementData, reason = RSSBuilder.SignLayout.ElementData.new( {
        eIndex = eIndex,
        mElementType = "Text",
        mSharedData = mSharedData,
        mTextData = mTextData,
    } )
    if mElementData == nil then
        computer.panic( "Could not create SignLayout: " .. reason or "" )
        return false
    end

    local result, reason = layout:addElement( mElementData )
    if not result then
        computer.panic( "Could not create SignLayout: " .. reason or "" )
        return false
    end

    return true
end


---Create an Image Element for an RSSBuilder SignLayout
RSSBuilder.addRSSImageElementToLayout = function( layout, eIndex, mElementName, mZIndex, mPosition, mTexture, mColourOverwrite, mOverwriteImageSize, mUse9SliceMode, mUrl, mImageSize, mRotation )
    local mIsUsingCustom = ( mUrl ~= nil )and( type( mUrl ) == "string" )and( mUrl ~= '' )
    if mIsUsingCustom == false then mIsUsingCustom = nil end

    local mSharedData, reason = RSSBuilder.SignLayout.ElementData.SharedData.new( {
        mElementName = mElementName,
        mPosition = mPosition,
        mZIndex = mZIndex,
        mTexture = mTexture,
        mColourOverwrite = mColourOverwrite,
        mUrl = mUrl,
        mIsUsingCustom = mIsUsingCustom,
        mRotation = mRotation,
    } )
    if mSharedData == nil then
        computer.panic( "Could not create SignLayout: " .. reason or "" )
    end

    local mImageData, reason = RSSBuilder.SignLayout.ElementData.ImageData.new(
        {
            mImageSize = mImageSize,
            mOverwriteImageSize = mOverwriteImageSize,
            mUse9SliceMode = mUse9SliceMode,
        } )
    if mImageData == nil then
        computer.panic( "Could not create SignLayout: " .. reason or "" )
    end

    local mElementData, reason = RSSBuilder.SignLayout.ElementData.new( {
        eIndex = eIndex,
        mElementType = "Image",
        mSharedData = mSharedData,
        mImageData = mImageData,
    } )
    if mElementData == nil then
        computer.panic( "Could not create SignLayout: " .. reason or "" )
    end

    local result, reason = layout:addElement( mElementData )
    if not result then
        computer.panic( "Could not create SignLayout: " .. reason or "" )
    end

    return true
end


---Generate the SignLayout and update the UIO Elements.  Note, this will explicitly access the RSS Sign Elements through the raw API instead of through a UIO Combinator as we want to be able to update the status before the combinator will be created.
---@param signs table The RSSSigns
---@param signLayout table The layout to be applied
---@param rssEidBlackground integer The index of the blackground; any extra elements will be hidden behind the blackground
---@param rssEZIndexHidden integer The z-index to be applied to objects that are to be hidden behind the blackground
RSSBuilder.refreshRSSSignsFromLayout = function( signs, signLayout, rssEidBlackground, rssEZIndexHidden )
    if signs == nil or #signs == 0 then return end

    for _, sign in pairs( signs ) do
        -- Enforce the layout of the main elements
        signLayout:apply( sign, false )

        -- Quickly hide extra elements behind the blackground
        if rssEidBlackground ~= nil then
            if rssEZIndexHidden == nil then
                computer.panic( "Nil z-index received for blackground" )
            end
            for eid = rssEidBlackground + 1, sign:GetNumOfElements() - 1 do
                sign:Element_SetZIndex( rssEZIndexHidden, eid )
            end
        end
    end
end


return RSSBuilder
