




-- local Color = require( "/lib/Colors.lua", EEPROM.Remote.CommonLib )
-- local ClassGroup = require( "/lib/classgroups.lua", EEPROM.Remote.CommonLib )
local UIO = require( "/UIO/UIO.lua", EEPROM.Remote.CommonLib )
require( "/UIO/UIOElements/Combinator.lua", EEPROM.Remote.CommonLib )
require( "/UIO/UIOElements/RSSElement.lua", EEPROM.Remote.CommonLib )
-- local RSSBuilder = require( "/lib/RSSBuilder.lua", EEPROM.Remote.CommonLib )
-- local Vector2f = require( "/lib/Vector2f.lua", EEPROM.Remote.CommonLib )


--------------------------------------------------------------------------------


---Create an RSSElement or panic
---@param sign userdata The RSSSign
---@param index number The index of the element on the sign
---@param zOffset number Optional zOffset to apply when setZIndex is called on the RSSElement
---@return UIO.UIOElements.RSSElement|nil
UIO.createRSSElement = function ( sign, index, zOffset )
    local uio = UIO.UIOElements.RSSElement.create( sign, index )
    if uio == nil then
        computer.panic( "Internal Software Error: Unable to create UIO.UIOElements.RSSElement - check params at the callsite" )
    end
    uio.zOffset = zOffset
    return uio
end


---Create a basic RSSElement
---@param signs table The RSSSigns
---@param index integer The index of the Element on the sign
---@return UIO.UIOElements.Combinator
UIO.createSimpleDisplayCombinator = function ( signs, index )
    local combined = {}

    for _, sign in pairs( signs ) do
        local uio = UIO.createRSSElement( sign, index,  0 )
        table.insert( combined, uio )
    end

    return UIO.UIOElements.Combinator.create( combined )
end


return UIO