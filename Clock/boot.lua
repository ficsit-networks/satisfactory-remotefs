Module = { Version = { full = { 1, 0, 1, '' } } }

---
--- Created by Rostriano
--- Date: 2024-07-07
---
--- Based on work by Aider
---

--- Use the nickname on the Computer to set the settings:
--- screen="mystring"        -- nick used to find the screen; defaults to "clock" if unset
---
--- Set the same nickname on the screen:
--- mystring
---
--- Define the timezone to be displayed on the screen by adding the tz config parameter to the screen's nick:
--- tz="UTC"    or    tz="M2CT"
--

require( "/lib/strict.lua", EEPROM.Remote.CommonLib )

local ClassGroup = require( "/lib/classgroups.lua", EEPROM.Remote.CommonLib )

-- These dimensions are optimal for a screen that has the smallest height possible and is one click wider than the minimum possible width
SCREEN_SIZE = { x = 7, y = 1 }


function initFromConfig()
  local gpus = computer.getPCIDevices( classes.GPU_T1_C )
  if gpus == nil then
    error( "No GPU T1 found" )
  end

  local screenNick = EEPROM.Boot.ComputerSettings[ "screen" ] or "clock"
  local screens = component.getComponentsByClassAndNick( ClassGroup.Displays.Screens.All, screenNick )
  if screens == nil then
    error( "No screens found with the 'clock' nick" )
  end

  if #gpus < #screens then
    computer.panic( 'Not enough GPUs to drive all the screens: ' .. #gpus .. ' vs ' .. #screens )
  end

  local screenDefinitions =  {}
  for i, screen in pairs( screens ) do
    local timeDisplayFunction = displayM2CT
    local settings = EEPROM.Settings.FromComponentNickname( screen, true )
    if rawget( settings, 'tz' ) == 'UTC' then
      timeDisplayFunction = displayUTC
    end

    table.insert( screenDefinitions, {
      gpu = gpus[i],
      screen = screen,
      display = timeDisplayFunction,
    })
  end

  return screenDefinitions
end

function initScreens( screenDefinitions )
  for _, screenDefinition in pairs( screenDefinitions ) do
    screenDefinition.gpu:bindScreen( screenDefinition.screen )
    screenDefinition.gpu:setSize( SCREEN_SIZE.x, SCREEN_SIZE.y )
    screenDefinition.gpu:setForeground( 1, 1, 1, 1 )
    screenDefinition.gpu:setBackground( 0, 0, 0, 0 )
  end
end

function clearScreens( screenDefinitions )
  for _, screenDefinition in pairs( screenDefinitions ) do
    screenDefinition.gpu:setSize( SCREEN_SIZE.x, SCREEN_SIZE.y )
    screenDefinition.gpu:fill( 0, 0, SCREEN_SIZE.x, SCREEN_SIZE.y, " " )
    screenDefinition.gpu:flush()
  end
end

function updateScreens( screenDefinitions )
  for _, screenDefinition in pairs( screenDefinitions ) do
    screenDefinition:display()
  end
end

function formatTime( timestamp )
  return string.format(
    "%2d:%02d",
    math.floor( timestamp / 3600 ) % 24,
    math.floor( timestamp / 60 ) % 60
  )
end

function displayM2CT( screenDefinition )
  screenDefinition.gpu:setText( 1, 0, formatTime( computer.time() ) )
  screenDefinition.gpu:flush()
end

function displayUTC( screenDefinition )
  screenDefinition.gpu:setText( 1, 0, formatTime( computer.magicTime()) )
  screenDefinition.gpu:flush()
end

function init()
  local screenDefinitions = initFromConfig()
  clearScreens( screenDefinitions )
  initScreens( screenDefinitions )

  return screenDefinitions
end

local screenDefinitions = init()
while true do
  event.pull(0.8)
  updateScreens( screenDefinitions )
end
