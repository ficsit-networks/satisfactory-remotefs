---
--- Created by Rostriano
--- Date: 2024-07-13
---

local SizeLimitedList = {}
SizeLimitedList.__index = SizeLimitedList
createModule( 'srf.DataStructures.SizeLimitedList', SizeLimitedList )

function SizeLimitedList.new( maxSize )
  local self = setmetatable( {
    first = 0,
    maxSize = 10,
    currSize = 0,
    items = {},

    maxVal = nil,
    minVal = nil,


  }, SizeLimitedList )
  if maxSize ~= nil then
      self.maxSize = maxSize
  end

  return self
end

function SizeLimitedList:setSize( newSize )
    if self.currSize > newSize then
        local shrinkBy = self.currSize - newSize
        local newFirst = self.first + shrinkBy

        while self.first < newFirst do
            self.items[ self.first ] = nil
            self.first = self.first + 1
        end
    end

    self.currSize = math.min( self.currSize, newSize )
    self.maxSize = newSize
end

function SizeLimitedList:getSize()
    return self.currSize
end

function SizeLimitedList:getMaxSize()
    return self.maxSize
end

function SizeLimitedList:add( item )
    self.items[ self.first + self.currSize ] = item

    if self.currSize < self.maxSize then
        self.currSize = self.currSize + 1
    else
        self.items[ self.first ] = nil
        self.first = self.first + 1
    end

    self:updateMinMaxVals()
end

function SizeLimitedList:getMinVal( default )
    return self.minVal or default
end

function SizeLimitedList:getMaxVal( default )
    return self.maxVal or default
end

function SizeLimitedList:iterate( f )
    local index = self.first
    local sentinel = self.first + self.currSize

    while index < sentinel do
        f( self.items[ index ] )
        index = index + 1
    end
end

function SizeLimitedList:updateMinMaxVals()
    self.minVal = nil
    self.maxVal = nil

    self:iterate(
        function( currVal )
            if self.minVal == nil then
                self.minVal = currVal
            else
                self.minVal = math.min( self.minVal, currVal )
            end

            if self.maxVal == nil then
                self.maxVal = currVal
            else
                self.maxVal = math.max( self.maxVal, currVal )
            end
        end
    )
end
