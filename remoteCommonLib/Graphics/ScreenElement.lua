---
--- Created by Rostriano
--- Date: 2024-07-14
---

--[[
     Offers the regular GPU T2 drawing commands, translating
     the coordinates to a screen location.

     This serves as the base for a graphics library, allowing
     multiple elements to be added that will (re)draw when
     this Element is being (re)drawn
]]


local ScreenElement = {
    gpu = nil,
    position = nil,
    dimensions = nil,
    subElements = {},

    -- Helper functions at the bottom of this script
    reposition = nil,
}

function ScreenElement:new( o )
    o = o or {}
    self.__index = self
    setmetatable( o, self )

    return o
end

function ScreenElement:init( gpu, position, dimensions )
    self.gpu = gpu
    self.position = position
    self.dimensions = dimensions
end

function ScreenElement:addElement( e )
    if e == nil then
        return
    end

    table.insert( self.subElements, e )
end

-- Draw the element and all sub elements that have been added
-- Remember to do a gpu:flush() later; this will make the gpu
-- clear the screen and then actually draw the elements
function ScreenElement:draw()
    print( "Repainting" )
    for _, element in pairs(self.subElements) do
        element:draw()
    end
end

function ScreenElement:flush()
    computer.error( 'ScreenElement:flush() should not be called; call draw(), then do a gpu:flush()' )
end

function ScreenElement:measureText( Text, Size, bMonospace )
    return self.gpu:measureText( Text, Size, bMonospace )
end

--- Flushes all draw calls to the visible draw call buffer to show all changes at once. The draw buffer gets cleared afterwards.
-- function ScreenElement:flush()
--     self.gpu:flush()
-- end

--- Draws some Text at the given position (top left corner of the text), text, size, color and rotation.
---@param position Vector2D @The position of the top left corner of the text.
---@param text string @The text to draw.
---@param size number @The font size used.
---@param color Color @The color of the text.
---@param monospace boolean @True if a monospace font should be used.
function ScreenElement:drawText( position, text, size, color, monospace )
    self.gpu:drawText(
        self:reposition( position ),
        text,
        size,
        color,
        monospace
    )
end

--- Draws a Spline from one position to another with given directions, thickness and color.
---@param start Vector2D @The local position of the start point of the spline.
---@param startDirections Vector2D @The direction of the spline of how it exists the start point.
---@param end Vector2D @The local position of the end point of the spline.
---@param endDirection Vector2D @The direction of how the spline enters the end position.
---@param thickness number @The thickness of the line drawn.
---@param color Color @The color of the line drawn.
function ScreenElement:drawSpline( start, startDirections, theEnd, endDirection, thickness, color )
    self.gpu:drawSpline(
        self:reposition( start ),
        startDirections,
        self:reposition( theEnd ),
        endDirection,
        thickness,
        color
    )
end

--- Draws a Rectangle with the upper left corner at the given local position, size, color and rotation around the upper left corner.
---@param position Vector2D @The local position of the upper left corner of the rectangle.
---@param size Vector2D @The size of the rectangle.
---@param color Color @The color of the rectangle.
---@param image string @If not empty string, should be image reference that should be placed inside the rectangle.
---@param rotation number @The rotation of the rectangle around the upper left corner in degrees.
function ScreenElement:drawRect( position, size, color, image, rotation )
    self.gpu:drawRect(
        self:reposition( position ),
        size,
        color,
        image,
        rotation
    )
end

--- Draws connected lines through all given points with the given thickness and color.
---@param points Vector2D[] @The local points that get connected by lines one after the other.
---@param thickness number @The thickness of the lines.
---@param color Color @The color of the lines.
function ScreenElement:drawLines( points, thickness, color )
    if #points < 2 then
       return
    end

    local newPoints = {}

    for _, currPoint in pairs( points ) do
        table.insert( newPoints, self:reposition( currPoint ) )
    end

    self.gpu:drawLines(
        newPoints,
        thickness,
        color
    )
end

--- Draws a box.
---@param boxSettings GPUT2DrawCallBox @The settings of the box you want to draw.
-- function ScreenElement:drawBox( boxSettings )
-- end

--- Draws a Cubic Bezier Spline from one position to another with given control points, thickness and color.
---@param p0 Vector2D @The local position of the start point of the spline.
---@param p1 Vector2D @The local position of the first control point.
---@param p2 Vector2D @The local position of the second control point.
---@param p3 Vector2D @The local position of the end point of the spline.
---@param thickness number @The thickness of the line drawn.
---@param color Color @The color of the line drawn.
function ScreenElement:drawBezier( p0, p1, p2, p3, thickness, color )
    self.gpu:drawBezier(
        self:reposition( p0 ),
        self:reposition( p1 ),
        self:reposition( p2 ),
        self:reposition( p3 ),
        -- self:scale1e( thickness ),
        thickness,
        color
    )
end


--[[
    Helper functions
]]

function ScreenElement:reposition( vector )
    return Vector2d.new(
        self.position.x + vector.x,
        self.position.y + vector.y
    )
end

createModule( 'srf.Graphics.ScreenElement', ScreenElement )
