---
--- Created by Rostriano
--- Date: 2024-08-09
---

require( "/Graphics/ScreenElement.lua", EEPROM.Remote.CommonLib )

local Plotter = srf.Graphics.ScreenElement:new()
createModule( 'srf.Graphics.Graph.Plotter', Plotter )


Plotter.__index = Plotter
Plotter.graph = nil -- The graph this plotter belongs to
Plotter.maxVal = nil
Plotter.scaleFactorX = nil
Plotter.color = Color.GREY_0500
Plotter.color = Color.GREY_0500
Plotter.lineThickness = 10
Plotter.dataSource = {}


function Plotter.new( o )
    local plotter = setmetatable( o or {}, Plotter )

    if o ~= nil and rawget( o, 'dataSource' ) ~= nil then
        plotter:setDataSource( o.dataSource )
    end

    return plotter
end

function Plotter:setDataSource( dataSource )
    self.dataSource = dataSource
    self.scaleFactorX = self.graph.dimensions.x / ( self.dataSource:getMaxSize() - 1 )
end

function Plotter:setColor( color )
    self.color = color
end

function Plotter:setLineThickness( lineThickness )
    self.lineThickness = lineThickness
end

function Plotter:draw()
    local i = 0
    local points = {}
    self.dataSource:iterate(
        function( currVal )
            local xPos = ( i + self.dataSource.maxSize - self.dataSource.currSize ) * self.scaleFactorX
            local yPos = self.graph.dimensions.y - currVal * self.graph.scaleFactorY

            local position = Vector2d.new( xPos, yPos )
            table.insert( points, position )
            i = i + 1
        end
    )

    self:drawLines( points, self.lineThickness, self.color )
end

-- function Plotter:scaleX( nrPointsHorizontally )
--     self.nrPointsHorizontally = nrPointsHorizontally
-- end

-- function Plotter:scaleY( nrPointsVertically )
--     self.nrPointsVertically = nrPointsVertically
-- end
