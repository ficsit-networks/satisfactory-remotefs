---
--- Created by Rostriano
--- Date: 2024-07-14
---

require( "/Graphics/ScreenElement.lua", EEPROM.Remote.CommonLib )
require( "/Graphics/Graph/Plotter.lua", EEPROM.Remote.CommonLib )

local Graph = srf.Graphics.ScreenElement:new()

Graph.__index = Graph
Graph.scaleFactorY = nil
Graph.maxVal = nil
Graph.dimensions = nil
Graph.scaleMarginFactor = 0.2
Graph.dataSources = {}
Graph.plotters = {}

function Graph.new()
    return setmetatable( {}, Graph )
end

function Graph:addPlotter( name, config )
    local plotter = srf.Graphics.Graph.Plotter.new()
    self.plotters[ name ] = plotter

    if config ~= nil then
        self:configurePlotter( name, config )
    end
end

function Graph:configurePlotter( name, config )
    local plotter = self.plotters[ name ]
    for k, v in pairs( config ) do
        plotter[ k ] = v
    end
    plotter.graph = self

    if rawget( plotter, 'dataSource' ) ~= nil then
        plotter:setDataSource( config.dataSource )
        table.insert( self.dataSources, config.dataSource or {} )
    end
end

function Graph:setMaxVal( maxVal )
    self.maxVal = maxVal
    self.scaleFactorY = self.dimensions.y / maxVal
end

function Graph:setDimensions( dimensions )
    self.dimensions = dimensions

    for _,currItem in ipairs( self.dataSources ) do
        currItem.dimensions = dimensions
    end
end

-- function Graph:setData( rawData )
--     self.rawData = rawData
-- end

-- function Graph:setMaxVal( maxVal )
--     self.maxVal = maxVal
--     self.scaleFactorY = self.dimensions.y / maxVal
-- end

-- function Graph:setColor( color )
--     self.color = color
-- end

-- function Graph:setLineThickness( lineThickness )
--     self.lineThickness = lineThickness
-- end

function Graph:draw()
    if self.maxVal == nil then
        self:autoResize()
    end

    for _, plotter in pairs( self.plotters ) do
        plotter:draw()
    end
end

function Graph:autoResize()
    local maxVal = self:getMaxVal()

    if self.scaleFactorY == nil then
        return self:initScaleFactors( maxVal )
    end

    local maxDisplayableVal = self.dimensions.y / ( self.scaleFactorY or 0.00000000001 )
    if
        maxDisplayableVal < maxVal or
        maxDisplayableVal * self.scaleMarginFactor > maxVal
    then
        self:initScaleFactors( maxVal )
    end
end

function Graph:initScaleFactors( maxVal )
    if maxVal == nil then
        maxVal = 0.00000000001
    end

    self.scaleFactorY = self.dimensions.y / ( maxVal * ( 1 + self.scaleMarginFactor ) )
end

function Graph:getMaxVal()
    local maxVal = nil
    for _,currItem in ipairs( self.dataSources ) do
        maxVal = math.max( maxVal or currItem:getMaxVal(), currItem:getMaxVal() )
    end
    return maxVal
end

createModule( 'srf.Graphics.Graph.Graph', Graph )
