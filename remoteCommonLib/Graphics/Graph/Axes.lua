---
--- Created by Rostriano
--- Date: 2024-07-21
---

if _G[ "____Axes" ] ~= nil then return _G[ "____Axes" ] end

ScreenElement = require( "/Graphics/ScreenElement.lua", EEPROM.Remote.CommonLib )

local Axes = ScreenElement:new()
_G[ "____Axes" ] = Axes



return Axes
