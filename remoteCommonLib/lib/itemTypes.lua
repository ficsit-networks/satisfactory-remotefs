---
--- Created by Rostriano
--- Date: 2024-05-13
---
--- Constants for item types because magic strings are evil
---

if _G[ "____ItemType" ] ~= nil then return _G[ "____ItemType" ] end
local ItemType = {}
_G[ "____ItemType" ] = ItemType


function ItemType.compareTypes(itemType1, itemType2)
    return itemType1.internalPath == itemType2.internalPath
end


-- Parts
ItemType.AdaptiveControlUnit = findItem("Adaptive Control Unit")
ItemType.AiLimiter = findItem("AI Limiter")
ItemType.AlienDnaCapsule = findItem("Alien DNA Capsule")
ItemType.AlienProtein = findItem("AlienProtein")
ItemType.AlcladAluminumSheet = findItem("Alclad Aluminum Sheet")
ItemType.AluminaSolution = findItem("Alumina Solution")
ItemType.AluminumCasing = findItem("Aluminum Casing")
ItemType.AluminumIngot = findItem("Aluminum Ingot")
ItemType.AluminumScrap = findItem("Aluminum Scrap")
ItemType.AutomatedWiring = findItem("Automated Wiring")
ItemType.Biomass = findItem("Biomass")
ItemType.BlackPowder = findItem("BlackPowder")
ItemType.Cable = findItem("Cable")
ItemType.CateriumIngot = findItem("Caterium Ingot")
ItemType.CircuitBoard = findItem("Circuit Board")
ItemType.ColorCartridge = findItem("Color Cartridge")
ItemType.CompactedCoal = findItem("Compacted Coal")
ItemType.Computer = findItem("Computer")
ItemType.Concrete = findItem("Concrete")
ItemType.CopperIngot = findItem("Copper Ingot")
ItemType.CopperSheet = findItem("Copper Sheet")
ItemType.CrystalOscillator = findItem("Crystal Oscillator")
ItemType.EmptyCanister = findItem("Empty Canister")
ItemType.EncasedIndustrialBeam = findItem("Encased Industrial Beam")
ItemType.Fabric = findItem("Fabric")
ItemType.FancyFireworks = findItem("FancyFireworks")
ItemType.FloppyDisk = findItem("Floppy Disk")
ItemType.Fuel = findItem("Fuel")
ItemType.GasFilter = findItem("Gas Filter")
ItemType.HardDriveT1 = findItem("Hard Drive T1")
ItemType.HeavyModularFrame = findItem("Heavy Modular Frame")
ItemType.HeavyOilResidue = findItem("Heavy Oil Residue")
ItemType.HighSpeedConnector = findItem("High-Speed Connector")
ItemType.IronIngot = findItem("Iron Ingot")
ItemType.IronPlate = findItem("Iron Plate")
ItemType.IronRebar = findItem("Iron Rebar")
ItemType.IronRod = findItem("Iron Rod")
ItemType.LuaEeprom = findItem("Lua EEPROM")
ItemType.LiquidBiofuel = findItem("Liquid Biofuel")
ItemType.ModularEngine = findItem("Modular Engine")
ItemType.ModularFrame = findItem("Modular Frame")
ItemType.Motor = findItem("Motor")
ItemType.Nobelisk = findItem("Nobelisk")
ItemType.PackagedAluminaSolution = findItem("Packaged Alumina Solution")
ItemType.PackagedFuel = findItem("Packaged Fuel")
ItemType.PackagedHeavyOilResidue = findItem("Packaged Heavy Oil Residue")
ItemType.PackagedLiquidBiofuel = findItem("Packaged Liquid Biofuel")
ItemType.PackagedOil = findItem("Packaged Oil")
ItemType.PackagedTurbofuel = findItem("Packaged Turbofuel")
ItemType.PackagedWater = findItem("Packaged Water")
ItemType.PetroleumCoke = findItem("Petroleum Coke")
ItemType.PhotovoltaicCell = findItem("Photovoltaic Cell")
ItemType.Plastic = findItem("Plastic")
ItemType.PolymerResin = findItem("PolymerResin")
ItemType.PowerShard = findItem("Power Shard")
ItemType.QuartzCrystal = findItem("Quartz Crystal")
ItemType.Quickwire = findItem("Quickwire")
ItemType.RadioControlUnit = findItem("Radio Control Unit")
ItemType.ReinforcedIronPlate = findItem("Reinforced Iron Plate")
ItemType.RifleAmmo = findItem("Rifle Ammo")
ItemType.Rotor = findItem("Rotor")
ItemType.Rubber = findItem("Rubber")
ItemType.Screw = findItem("Screw")
ItemType.Silica = findItem("Silica")
ItemType.SmartPlating = findItem("SmartPlating")
ItemType.SmokelessPowder = findItem("Smokeless Powder")
ItemType.SolidBiofule = findItem("Solid Biofule")
ItemType.SparklyFireworks = findItem("Sparkly Fireworks")
ItemType.Stator = findItem("Stator")
ItemType.SteelBeam = findItem("Steel Beam")
ItemType.SteelIngot = findItem("Steel Ingot")
ItemType.SteelPipe = findItem("Steel Pipe")
ItemType.StunRebar = findItem("Stun Rebar")
ItemType.Supercomputer = findItem("Supercomputer")
ItemType.SweetFireforks = findItem("Sweet Fireforks")
ItemType.Turbofuel = findItem("Turbofuel")
ItemType.VersatileFramework = findItem("Versatile Framework")
ItemType.Wire = findItem("Wire")

-- Equipment
ItemType.Beacon = findItem("Beacon")
ItemType.BladeRunners = findItem("Blade Runners")
ItemType.Chainsaw = findItem("Chainsaw")
ItemType.GasMask = findItem("GasMask")
ItemType.Jetpack = findItem("Jetpack")
ItemType.MedicinalInhaler = findItem("Medicinal Inhaler")
ItemType.MicroLabelTool = findItem("Micro Label Tool")
ItemType.NetworkManager = findItem("Network Manager")
ItemType.NobeliskDetonator = findItem("Nobelisk Detonator")
ItemType.ObjectScanner = findItem("Object Scanner")
ItemType.PortableMiner = findItem("Portable Miner")
ItemType.RebarGun = findItem("Rebar Gun")
ItemType.Rifle = findItem("Rifle")
ItemType.XenoBasher = findItem("Xeno-Basher")
ItemType.XenoZapper = findItem("Xeno-Zapper")
ItemType.Zipline = findItem("Zipline")


-- for _, itemType in pairs(ItemType) do
--     if type(itemType) ~= "userdata" then
--         goto continue
--     end
--     print(itemType.name)
--     ::continue::
-- end
-- computer.stop()

return ItemType