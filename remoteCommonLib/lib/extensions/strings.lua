-- string class extension functions



---Split a string that is deliminated by the separator
---@param str string String to split
---@param sep string? Separator to split string by; default: ','
---@return table: Array of split string elements
function string.split( str, sep )
    local pattern = string.format( '([^%s]+)', sep or ',' )

    local results = {}
    local _ = string.gsub( str, pattern,
        function( c )
            results[ #results + 1 ] = c
        end
    )

    return results
end


---Truncate a string to the given length, if it's longer than the given length
---@param str string String to truncate
---@param maxLength integer Maximum length of the string
---@return string: The truncated string, or the original string if it's no longer than length
function string.truncate( str, maxLength )
    if string.len( str ) <= maxLength then
        return str
    end

    return string.sub( str, 1, maxLength )
end
