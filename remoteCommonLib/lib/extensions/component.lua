-- component class extension functions


require( "/lib/extensions/tables.lua", EEPROM.Remote.CommonLib )


---Find and return a table of all the NetworkComponent proxies that are of the given class[es]
---@param class any Class name or table (of tables) of class names
---@param boolean Return only one
---@return table | nil | proxy: indexed table of all NetworkComponents found
function component.getComponentsByClass( class, getOne )
    local results = {}

    if ( getOne == nil ) then
        getOne = false
    end


    if type( class ) == "table" then

        for _, c in pairs( class ) do
            local proxies = component.getComponentsByClass( c, getOne )
            if not getOne then
                table.concat( results, proxies )
            else
                if( proxies ~= nil ) then
                    return proxies
                end
            end
        end

    elseif type( class ) == "string" then

        local ctype = classes[ class ]
        if ctype ~= nil then
            local comps = component.findComponent( ctype )
            for _, c in pairs( comps ) do
                local proxy = component.proxy( c )
                if getOne and proxy ~= nil then
                    return proxy
                elseif not table.hasValue( results, proxy ) then
                    table.insert( results, proxy )
                end
            end
        end

    end

    if ( getOne ) then
        return {}
    end

    return results
end


---Find and return a table of all the NetworkComponent proxies that are of the given class[es] and contain the given nick parts
---@param class any Class name or table (of tables) of class names
---@param class nickParts Nick or parts of a nick that we want to see
---@return table: indexed table of all NetworkComponents found
function component.getComponentsByClassAndNick( class, nickParts )

    --
    -- TODO: rewrite so that FIN does most work for us:
    --      local comps1, comps2 = component.findComponent("test north", "okay")
    --
    if type( nickParts ) == 'string' then
        nickParts = { nickParts }
    end

    local classComponents = component.getComponentsByClass( class )
    local results = {}

    for _, component in pairs( classComponents ) do
        for _, nickPart in pairs( nickParts ) do
            if component.nick:find( nickPart, 1, true ) == nil then
                goto nextComponent
            end
        end

        table.insert( results, component )

        ::nextComponent::
    end

    return results
end



-- Storage name constants --
component.INV_INVENTORY     = 'Inventory'
component.INV_STORAGE       = 'StorageInventory'
component.INV_INPUT         = 'InputInventory'
component.INV_OUTPUT        = 'OutputInventory'
component.INV_POTENTIAL     = 'InventoryPotential'

---Get an inventories index by its internal name.  Why?  Because not everything with an inventory uses the same index but they use the same name.
function component.getInventoryIndexByName( proxy, name )
    if proxy == nil then return nil end
    if type( proxy ) ~= "userdata" then return nil end
    local gi = proxy[ "getInventories" ]
    if gi == nil or type( gi ) ~= "function" then return nil end
    local inventories = gi( proxy )
    for idx = 1, #inventories do
        local inv = inventories[ idx ]
        if inv.internalName == name then return idx end
    end
    return nil
end

---Get an inventory by its internal name instead of index.  Why?  Because not everything with an inventory uses the same index but they use the same name.
function component.getInventoryByName( proxy, name )
    if proxy == nil or type( proxy ) ~= "userdata" then return nil end
    local gi = proxy[ "getInventories" ]
    if gi == nil or type( gi ) ~= "function" then return nil end
    local inventories = gi( proxy )
    --print( proxy.internalName )
    for _, inv in pairs( inventories ) do
        --print( _, "'" .. inv.internalName .. "'" )
        if inv.internalName == name then return inv end
    end
    return nil
end

